package frazeusz.monitor;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.DynamicTimeSeriesCollection;
import org.jfree.data.time.Second;

import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;

public class TimeSeriesChart{

    private static final int TIME_RANGE = 60;
    private JPanel panel;
    private final DynamicTimeSeriesCollection dataSeries;

    public TimeSeriesChart(String name, String xAxisName, String yAxisName){
        this.dataSeries = new DynamicTimeSeriesCollection(1, TIME_RANGE, new Second());
        dataSeries.setTimeBase(new Second());
        float[] dataset = new float[TIME_RANGE];
        dataSeries.addSeries(dataset, 0, "chart");

        JFreeChart chart = ChartFactory.createTimeSeriesChart(
                name,  // title
                xAxisName,             // x-axis label
                yAxisName,   // y-axis label
                dataSeries,            // data
                false,               // create legend?
                false,               // generate tooltips?
                false               // generate URLs?
        );
        Font titleFont = new Font("Consolas", Font.BOLD, 13);
        chart.getTitle().setFont(titleFont);
        int a = 238;
        chart.setBackgroundPaint(new Color(a, a, a));

        XYPlot plot = chart.getXYPlot();

        plot.setBackgroundPaint(new Color(15, 15, 15));
        plot.setDomainGridlinePaint(new Color(200, 200, 200));
        plot.setRangeGridlinePaint(new Color(200, 200, 200));

        Font axisLabelFont = new Font("Consolas", Font.TRUETYPE_FONT, 11);
        plot.getDomainAxis().setLabelFont(axisLabelFont);
        plot.getRangeAxis().setLabelFont(axisLabelFont);

        Font axisFont = new Font("Consolas", Font.PLAIN, 8);
        plot.getDomainAxis().setTickLabelFont(axisFont);
        plot.getRangeAxis().setTickLabelFont(axisFont);

        XYItemRenderer r = plot.getRenderer();
        if (r instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;
            renderer.setBaseShapesVisible(false);
            renderer.setBaseShapesFilled(false);
            renderer.setSeriesPaint(0, new Color(0, 180, 0));
            renderer.setSeriesStroke(0, new BasicStroke(1f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
        }

        DateAxis axis = (DateAxis) plot.getDomainAxis();
        axis.setDateFormatOverride(new SimpleDateFormat("H:mm:ss"));

        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setMouseZoomable(false);
        chartPanel.setPreferredSize(new Dimension(350, 250));

        panel = new JPanel();
        panel.add(chartPanel);
    }

    public JPanel getPanel(){
        return panel;
    }

    public void addDataAndUpdate(float x){
        float[] tmp = new float[1];
        tmp[0] = x;
        dataSeries.advanceTime();
        dataSeries.appendData(tmp);
    }

}
