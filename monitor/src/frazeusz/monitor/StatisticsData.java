package frazeusz.monitor;

import javax.swing.*;
import java.awt.*;

public class StatisticsData {

    private JPanel panel;
    private JTextField actualLabelVal;
    private JTextField averageLabelVal;
    private JTextField totalLabelVal;

    public StatisticsData(String unit) {
        this.panel = new JPanel(new GridLayout(3, 1));
        panel.setPreferredSize(new Dimension(350, 50));
        Font font = new Font("Consolas", Font.BOLD, 12);

        JPanel p1 = new JPanel();
        p1.setLayout(new BoxLayout(p1, BoxLayout.LINE_AXIS));
        JTextField actualLabel = new JTextField("Wartość aktualna:");
        actualLabel.setEditable(false);
        actualLabel.setBorder(BorderFactory.createEmptyBorder());
        actualLabel.setHorizontalAlignment(JTextField.LEFT);
        actualLabel.setFont(font);

        actualLabelVal = new JTextField("0.00");
        actualLabelVal.setEditable(false);
        actualLabelVal.setBorder(BorderFactory.createEmptyBorder());
        actualLabelVal.setHorizontalAlignment(JTextField.RIGHT);
        actualLabelVal.setFont(font);

        JTextField actualLabelUnit = new JTextField(" " + unit + "/s");
        actualLabelUnit.setEditable(false);
        actualLabelUnit.setBorder(BorderFactory.createEmptyBorder());
        actualLabelUnit.setHorizontalAlignment(JTextField.LEFT);
        actualLabelUnit.setFont(font);
        p1.add(Box.createRigidArea(new Dimension(40, 0)));
        p1.add(actualLabel);
        p1.add(actualLabelVal);
        p1.add(actualLabelUnit);

        JPanel p2 = new JPanel();
        p2.setLayout(new BoxLayout(p2, BoxLayout.LINE_AXIS));
        JTextField averageLabel = new JTextField("Wartość średnia:");
        averageLabel.setEditable(false);
        averageLabel.setBorder(BorderFactory.createEmptyBorder());
        averageLabel.setHorizontalAlignment(JTextField.LEFT);
        averageLabel.setFont(font);

        averageLabelVal = new JTextField("0.00");
        averageLabelVal.setEditable(false);
        averageLabelVal.setBorder(BorderFactory.createEmptyBorder());
        averageLabelVal.setHorizontalAlignment(JTextField.RIGHT);
        averageLabelVal.setFont(font);

        JTextField averageLabelUnit = new JTextField(" " + unit + "/s");
        averageLabelUnit.setEditable(false);
        averageLabelUnit.setBorder(BorderFactory.createEmptyBorder());
        averageLabelUnit.setHorizontalAlignment(JTextField.LEFT);
        averageLabelUnit.setFont(font);
        p2.add(Box.createRigidArea(new Dimension(40, 0)));
        p2.add(averageLabel);
        p2.add(averageLabelVal);
        p2.add(averageLabelUnit);

        JPanel p3 = new JPanel();
        p3.setLayout(new BoxLayout(p3, BoxLayout.LINE_AXIS));
        JTextField totalLabel = new JTextField("Wartość całkowita:");
        totalLabel.setEditable(false);
        totalLabel.setBorder(BorderFactory.createEmptyBorder());
        totalLabel.setHorizontalAlignment(JTextField.LEFT);
        totalLabel.setFont(font);

        totalLabelVal = new JTextField("0.00");
        totalLabelVal.setEditable(false);
        totalLabelVal.setBorder(BorderFactory.createEmptyBorder());
        totalLabelVal.setHorizontalAlignment(JTextField.RIGHT);
        totalLabelVal.setFont(font);

        JTextField totalLabelUnit = new JTextField(" " + unit);
        totalLabelUnit.setEditable(false);
        totalLabelUnit.setBorder(BorderFactory.createEmptyBorder());
        totalLabelUnit.setHorizontalAlignment(JTextField.LEFT);
        totalLabelUnit.setFont(font);
        p3.add(Box.createRigidArea(new Dimension(40, 0)));
        p3.add(totalLabel);
        p3.add(totalLabelVal);
        p3.add(totalLabelUnit);
        p3.add(Box.createRigidArea(new Dimension(20, 0)));

        panel.add(p1);
        panel.add(p2);
        panel.add(p3);
    }

    public JPanel getPanel() {
        return this.panel;
    }

    public void setAllValues(float actual, float average, float total) {
        actualLabelVal.setText(String.format("%8.2f", actual));
        averageLabelVal.setText(String.format("%8.2f", average));
        totalLabelVal.setText(String.format("%8.2f", total));
    }
}
