package frazeusz.monitor;

import frazeusz.crawler.CrawlerStatistics;
import frazeusz.patternMatcher.IPatternMatcherMonitor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Monitor implements Runnable, ActionListener {
    private static final int interval = 1000;

    private final TimeSeriesChart bps_chart;
    private final TimeSeriesChart pps_chart;
    private final TimeSeriesChart bpps_chart;

    private final StatisticsData bps_data;
    private final StatisticsData pps_data;
    private final StatisticsData bpps_data;

    private float bps_total = 0.0f;
    private long pps_total = 0L;
    private float bpps_total = 0.0f;

    private long steps = 0;

    CrawlerStatistics crawlerStatistics;
    IPatternMatcherMonitor patternMatcher;

    public Monitor(CrawlerStatistics crawler, IPatternMatcherMonitor patternMatcher) {
        this.bps_chart = new TimeSeriesChart("  Szybkość pobierania danych", "czas", "KB/s");
        this.pps_chart = new TimeSeriesChart("      Szybkość pobierania stron", "czas", "Ilość stron");
        this.bpps_chart = new TimeSeriesChart("    Szybkość przetwarzania danych", "czas", "KB/s");

        this.bps_data = new StatisticsData("KB");
        this.pps_data = new StatisticsData("stron");
        this.bpps_data = new StatisticsData("KB");

        this.crawlerStatistics = crawler;
        this.patternMatcher = patternMatcher;
    }

    public JPanel getPanel() {
        JPanel charts = new JPanel();
        charts.setLayout(new BoxLayout(charts, BoxLayout.LINE_AXIS));
        charts.setPreferredSize(new Dimension(1050, 250));
        charts.add(bps_chart.getPanel());
        charts.add(pps_chart.getPanel());
        charts.add(bpps_chart.getPanel());

        JPanel statistics = new JPanel();
        statistics.setLayout(new BoxLayout(statistics, BoxLayout.LINE_AXIS));
        statistics.setPreferredSize(new Dimension(1050, 50));

        statistics.add(bps_data.getPanel());
        statistics.add(pps_data.getPanel());
        statistics.add(bpps_data.getPanel());

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        panel.setPreferredSize(new Dimension(1050, 300));
        panel.add(charts);
        panel.add(statistics);

        return panel;
    }

    public void startMonitor() {
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        Timer timer = new Timer(interval, this);
        timer.start();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        steps++;
        long bps;
        long pps;
        long bpps;
        try {
            bps = crawlerStatistics.getBytes();
        } catch (Exception exc) {
            bps = 0L;
        }
        try {
            pps = crawlerStatistics.getPages();
        } catch (Exception exc) {
            pps = 0L;
        }
        try {
            bpps = patternMatcher.getBytes();
        } catch (Exception exc) {
            bpps = 0L;
        }

        float bpsInKB = ((float) (bps)) / 1024;
        float bppsInKB = ((float) (bpps)) / 1024;

        float bps_act = bpsInKB - bps_total;
        float pps_act = (float) (pps - pps_total);
        float bpps_act = bppsInKB - bpps_total;

        bps_total = bpsInKB;
        pps_total = pps;
        bpps_total = bppsInKB;

        float bps_avg = bps_total / steps;
        float pps_avg = ((float) pps_total) / steps;
        float bpps_avg = bpps_total / steps;

        bps_chart.addDataAndUpdate(bps_act);
        pps_chart.addDataAndUpdate(pps_act);
        bpps_chart.addDataAndUpdate(bpps_act);

        bps_data.setAllValues(bps_act, bps_avg, bps_total);
        pps_data.setAllValues(pps_act, pps_avg, (float) pps_total);
        bpps_data.setAllValues(bpps_act, bpps_avg, bpps_total);
    }

}
