package main;

import frazeusz.crawler.CrawlerStatistics;
import frazeusz.monitor.Monitor;
import frazeusz.patternMatcher.IPatternMatcherMonitor;
import frazeusz.patternMatcher.PatternMatcher;

import javax.swing.*;
import java.awt.*;

public class Main {

    public static void main(String args[]){
        System.out.println("Starting");
        CrawlerStatistics crawlerStatistics = new CrawlerStatistics();
        IPatternMatcherMonitor patternMatcher = new PatternMatcher();
        Monitor mon = new Monitor(crawlerStatistics, patternMatcher);
        JFrame frame = new JFrame("Monitor");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(1100, 350));
        frame.setBounds(150, 100, 1100, 350);
        frame.setResizable(false);
        frame.getContentPane().add(mon.getPanel());
        frame.pack();
        mon.startMonitor();
        frame.setVisible(true);
    }
}
