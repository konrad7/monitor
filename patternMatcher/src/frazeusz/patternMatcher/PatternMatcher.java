package frazeusz.patternMatcher;

import java.util.Random;

public class PatternMatcher implements IPatternMatcherMonitor{
    private long bytes = 0L;
    private Random r = new Random();

    public long getBytes(){
        bytes += (long)(r.nextDouble()*100000)+10000L;
        return bytes;
    }
}
