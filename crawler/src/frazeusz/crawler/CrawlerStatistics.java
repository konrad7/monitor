package frazeusz.crawler;

import java.util.Random;

public class CrawlerStatistics {
    private long bytes = 0L;
    private long pages = 0L;
    private Random r = new Random();

    public long getBytes(){
        bytes += (long)(r.nextDouble()*100000)+10000L;
        return bytes;
    }

    public long getPages(){
        pages += (long)(r.nextDouble()*100000)+10000L;
        return pages;
    }
}
